 <?php get_header(); ?>
 <section class="header-bottom">
 					<div class="slideshow-holder">
 						<nav class="slideshow-nav">
 							<div id="nav"></div>
 						</nav>
 						<div class="slideshow">
 							<?php
                 if ( $images = get_posts(array(
                     'post_parent' => $post->ID,
                     'post_type' => 'attachment',
                     'numberposts' => -1,
                     'orderby'        => 'title',
                     'order'           => 'ASC',
                     'post_mime_type' => 'image',
                     'exclude' => $thumb_ID,
                     )))
                 {
                     foreach( $images as $image ) {
                         $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                         echo '<img src="'.$attachmentImage[0].'">';
                     }
                 }
               ?>
 						</div>
 					</div><!--slideshow holder-->
 				</section><!--header bottom-->
 			</header><!--header-->
 			<section class="wrapper">
				<aside class="left-box">
					<?php get_sidebar() ?>
				</aside><!--left-box-->
				<article class="content-rooms">
				<?php query_posts('post_type=service&orderby=date&order=ASC'); while(have_posts()): the_post(); ?>
					<article class="service <?php the_field('block_type') ?>">
						<?php ob_start(); ?>
						<figure class="btn-img">
							<a href="<?php the_permalink() ?>"><img src="<?php the_field('thumb'); ?>" alt="<?php the_title() ?>"></a>
                            <a href="<?php the_permalink() ?>" class="<?php echo (get_field('block_type')=='spa2')?'btn-big-right':'btn-smoll-left-right'?>">Подробнее</a>
						</figure>
						<?php $btn_html = ob_get_clean(); ?>
						<?php if(get_field('block_type')!='spa2') echo $btn_html; ?>
						<article class="service-post">
							<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
							<p>
								<?php the_excerpt(); ?>
							</p>
						</article>
						<?php if(get_field('block_type')=='spa2') echo $btn_html; ?>
					</article>
					<?php endwhile; ?>
				</article><!--content-->
 			</section><!--wrapper-->
 		</section><!--page-->
 <?php get_footer(); ?>