<?php get_header(); ?>
<section class="header-bottom">
					<div class="slideshow-holder">
						<nav class="slideshow-nav">
							<div id="nav"></div>
						</nav>
						<div class="slideshow">
							 <?php
                if ( $images = get_posts(array(
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'orderby'        => 'title',
                    'order'           => 'ASC',
                    'post_mime_type' => 'image',
                    'exclude' => $thumb_ID,
                    )))
                {
                    foreach( $images as $image ) {
                        $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                        echo '<img src="'.$attachmentImage[0].'">';
                    }
                }
              ?>
						</div>
					</div><!--slideshow holder-->
				</section><!--header bottom-->
			</header><!--header-->
			<section class="wrapper">
				<aside class="left-box">
					<?php get_sidebar() ?>		
				</aside><!--left-box-->
				<article class="content-offers">
                    <?php function cycle(&$arr) {
                        $arr[] = array_shift($arr);
                        return end($arr);
                    }
                    $oddEven = array('even', 'odd');
                    ?>
				<?php query_posts("post_type=offer&orderby=date&order=ASC"); while(have_posts()): the_post(); ob_start(); ?>
				<article class="service-post">
                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                    <p>
                    <?php the_excerpt(); ?>
                    </p>
                </article>
        <?php 
        if(get_field('image_position')=='картинка справа' && get_field('block_type')=='spa') $btn_class = 'btn-big-right';
        if(get_field('image_position')=='картинка справа' && get_field('block_type')!='spa') $btn_class = 'btn-smoll-right';
        if(get_field('image_position')=='картинка слева') $btn_class = 'btn-smoll-left-right';
        ?>
        <?php $content_block = ob_get_clean(); ?>
					<article class="service <?php the_field('block_type') ?> <?php echo cycle($oddEven); ?>">
						<?php if(get_field('image_position')=='картинка справа') echo $content_block; ?>
						<figure class="btn-img">
							<img src="<?php the_field('thumb') ?>" alt="<?php the_title(); ?>">
							<a href="<?php the_permalink() ?>" class="<?php echo $btn_class ?>">Подробнее</a>
						</figure>
						<?php if(get_field('image_position')=='картинка слева') echo $content_block; ?>
					</article>
					<?php endwhile; ?>
				</article><!--content-->
			</section><!--wrapper-->
		</section><!--page-->
<?php get_footer(); ?>