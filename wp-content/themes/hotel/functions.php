<?php

function register_hotel_menus() {
  register_nav_menus(
    array( 'header-menu' => __( 'Header Menu' ) )
  );
}
add_action( 'init', 'register_hotel_menus' );


function theme_register_sidebars() {
    $args = array(
        'name'          => 'Блок на главной',
        'id'            => 'home-page-sidebar',
        'description'   => '',
        'class'         => '',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>' );

    register_sidebar( $args );
}

add_action( 'widgets_init', 'theme_register_sidebars' );

function register_post_types() {
  $labels = array(
		'name'               => 'Номера',
		'singular_name'      => 'Номер',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новый номер',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все номера',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти номер',
		'not_found'          => 'Не найдено номеров',
		'not_found_in_trash' => 'В Корзине нет номеров', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Номера'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Номера отеля',
		'public'        => false,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => array('slug' => 'rooms'),
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
	);
	register_post_type( 'room', $args );	
	
  $labels = array(
		'name'               => 'Фото',
		'singular_name'      => 'Фото',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новое фото',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все фото',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти фото',
		'not_found'          => 'Не найдено фото',
		'not_found_in_trash' => 'В Корзине нет фото', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Фото'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Фотографии',
		'public'        => false,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => array('slug' => 'photos'),
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title' ),
	);
	register_post_type( 'photo', $args );	

  $labels = array(
		'name'               => 'Услуги',
		'singular_name'      => 'Услуга',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новую услугу',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все услуги',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти услугу',
		'not_found'          => 'Не найдено услуг',
		'not_found_in_trash' => 'В Корзине нет услуг', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Услуги'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Услуги',
		'public'        => false,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => array('slug' => 'turns'),
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title', 'editor', 'thumbnail'),
	);
	register_post_type( 'turn', $args );	
	
	
	$labels = array(
		'name'               => 'Спецпредложения',
		'singular_name'      => 'Спецпредложение',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новое спецпредложение',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все спецпредложения',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти спецпредложения',
		'not_found'          => 'Не найдено спецпредложений',
		'not_found_in_trash' => 'В Корзине нет спецпредложений', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Предложения'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Спецпредложения в отеле',
		'public'        => true,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => array('slug' => 'offers'),
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
	);
	register_post_type( 'offer', $args );	
	
	$labels = array(
		'name'               => 'Бары',
		'singular_name'      => 'Бар',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новый бар',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все бары',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти бары',
		'not_found'          => 'Не найдено баров',
		'not_found_in_trash' => 'В Корзине нет баров', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Бары'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Бары и рестораны в отеле',
		'public'        => true,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
	);
	register_post_type( 'bar', $args );	
	
	$labels = array(
		'name'               => 'Сервисы',
		'singular_name'      => 'Сервис',
		'add_new'            => 'Создать',
		'add_new_item'       => 'Создать новый сервис',
		'edit_item'          => 'Изменить',
		'new_item'           => 'Создать',
		'all_items'          => 'Все сервисы',
		'view_item'          => 'Просмотреть',
		'search_items'       => 'Найти сервисы',
		'not_found'          => 'Не найдено сервисоы',
		'not_found_in_trash' => 'В Корзине нет сервисов', 
		'parent_item_colon'  => '',
		'menu_name'          => 'Сервисы'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Сервис в отеле',
		'public'        => true,
		'menu_position' => 5,
		'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'query_var' => true,
    'has_archive' => true,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
	);
	register_post_type( 'service', $args );	
}
add_action( 'init', 'register_post_types' );

add_theme_support( 'post-thumbnails', array( 'page', 'offer', 'bar', 'service', 'turn', 'room', 'post') );

//set_post_thumbnail_size( 190, 120 );

function sendPriceToClientScript()
{
    $price = get_option( 'hp' );
    if (empty($price)) {
        $price = 0;
    }
    echo "<script type=\"text/javascript\">var price = {$price}; theme_dir = '", get_template_directory_uri(), "';</script>";
}
add_action( 'wp_head', 'sendPriceToClientScript' );

/**
*
* Преобразование объекта в массив
*
* @param    object  $object преобразуемый объект
* @reeturn      array
*
*/
function objectToArray( $object )
{
	if ( ! is_object( $object ) && ! is_array( $object )) {
		return $object;
	}
	if ( is_object( $object )) {
		$object = get_object_vars( $object );
	}
	return array_map( 'objectToArray', $object );
}

function my_custom_admin_head()
{
	echo '<link rel="stylesheet" href="' . get_template_directory_uri() .'/js/jquery-ui/smoothness/jquery-ui-1.10.3.custom.min.css">';
}
add_action('admin_head', 'my_custom_admin_head');

/* Обрезка */
function trim_title_words($count, $after) {  
  $title = get_the_title();  
  $words = split(' ', $title);  
  if (count($words) > $count) {  
      array_splice($words, $count);  
      $title = implode(' ', $words);  
  }  
  else $after = '';  
  echo $title . $after;  
}
function trim_content_words($count, $after) {  
  $content = get_the_content();  
  $words = split(' ', $content);  
  if (count($words) > $count) {  
      array_splice($words, $count);  
      $content = implode(' ', $words);  
  }  
  else $after = '';  
  echo $content . $after;  
}