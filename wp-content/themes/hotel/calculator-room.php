<?php
$rooms = get_posts( array(
		'post_type' => 'room',
		'numberposts' => -1,
		'orderby' => 'date',
		'order' => 'ASC',
));
?>

<div class="section choice-room">
	<label for="hotel-room">
	    <select id="hotel-room" name="room">
			<?php foreach ( $rooms as $room ) : ?>
				<option value="<?php echo $room->ID ?>"><?php echo get_the_title( $room->ID ) ?></option>
			<?php endforeach ?>
		</select>
        <p>Категория номера</p>
	</label>
</div>