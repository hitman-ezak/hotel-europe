<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?php if (is_front_page()) echo "Home"; else wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mqueries.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slicknav.css" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/cycle.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.colorbox.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.selectbox.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hotel-calculator.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/hover.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.slicknav.min.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<script>
    $(function(){
        $('.menu').slicknav();
    });
</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="boxes">
  <div id="dialog" class="window">
    <h2>Спасибо!</h2>
    <p>В ближайшее время с вами свяжутся.</p>
      <div class="btn-search-box-holder">
          <div class="btn-search-l"></div>
          <div class="content">
              <a class="btn-search" href="#" onclick="$('#dialog').hide(); $('#mask').hide(); return false;">Закрыть</a>
          </div>
          <div class="btn-search-r"></div>
      </div>
  </div>
  <div id="warningModal" class="window">
    <h2>Ошибка</h2>
    <div class="content"></div>
      <div class="btn-search-box-holder">
          <div class="btn-search-l"></div>
          <div class="content">
              <a class="btn-search" href="#" onclick="$('#warningModal').hide(); $('#mask').hide(); return false;">Закрыть</a>
          </div>
          <div class="btn-search-r"></div>
      </div>
  </div>
  <div id="order" class="window">
    <div class="form-holder">
      <a class="close" href="#">close</a>
	  <div class="thank-you">
      <form class="how-to-contact" action="">
        <fieldset>
          <div class="form-title">
            <div class="form-title-l"></div>
              <div class="content">
                <h1>Как с Вами связаться?</h1>
                <span>* — обязательные поля</span>
              </div>
            <div class="form-title-r"></div>
          </div>
          <div class="row required_fields">
            <label for="name">Имя:</label>
            <input id="name" type="text">
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row required_fields">
            <label for="surname">Фамилия:</label>
            <input id="surname" type="text" >
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row required_fields">
            <label for="patronymic">Отчество:</label>
            <input id="patronymic" type="text" >
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row required_fields">
            <label for="country">Страна:</label>
            <input id="country" type="text" >
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row required_fields">
            <label for="phone-number">Контактный телефон:</label>
            <input id="phone-number" type="text" >
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row required_fields">
            <label for="email">Email:</label>
            <input id="email" type="text" >
            <span>Данное поле является обязательным</span>
          </div>
          <div class="row">
            <label for="coments">Комментарии:</label>
            <textarea id="coments"></textarea>
          </div>
          <div class="row">
            <div class="btn-search-holder">
              <div class="btn-search-box">
                <div class="btn-search-box-holder">
                  <div class="btn-search-l"></div>
                    <div class="content">
                      <!--input class="btn-search" type="submit" src="images/btn-search.png" value="Отправить" /-->
					  <a id="contact-submit" class="btn-search" href="#" name="modal" class="btn-contact">Отправить</a>
                    </div>
                  <div class="btn-search-r"></div>
                </div>
              </div>
            </div>
          </div>

        </fieldset>
      </form>

	  </div>
    <div class="row row-close">
        <div class="btn-search-holder">
            <div class="btn-search-box">
                <div class="btn-search-box-holder">
                    <div class="btn-search-l"></div>
                    <div class="content">
                        <a class="btn-search" href="#" class="btn-contact" onclick="$('#order').hide(); $('#mask').hide(); return false;">Закрыть</a>
                    </div>
                    <div class="btn-search-r"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
  </div>
  <div id="order-fill" class="window-order-fill">
      <div class="order-fill-inside">
        <div class="order-fill-row order-fill-row-date">
            <p>Выберите даты отъезда и приезда</p>
        </div>
        <div class="order-fill-row order-fill-row-people">
          <p>Выберите количество человек</p>
        </div>
        <div class="btn-search-box-holder">
            <div class="btn-search-l"></div>
            <div class="content">
                <a class="btn-search" href="#" onclick="$('#order-fill').hide(); $('#mask').hide(); return false;">Закрыть</a>
            </div>
            <div class="btn-search-r"></div>
        </div>
    </div>
  </div>
    <div id="order-fill-contact" class="window-order-fill">
        <div class="order-fill-inside">
            <p>Заполните все поля</p>
            <div class="btn-search-box-holder">
                <div class="btn-search-l"></div>
                <div class="content">
                    <a class="btn-search" href="#" onclick="$('#order-fill-contact').hide(); $('#mask').hide(); return false;">Закрыть</a>
                </div>
                <div class="btn-search-r"></div>
            </div>
        </div>
    </div>
  <script>
	 $('#order input').focus(function() {
	   $(this).next().css('visibility','hidden');
	   $(this).css('border', '');
	   });
	 $('.btn-search').click(function(){
	 	var roomID = $('#hotel-room').val();
	 	var inps=$("#order input");
		
	 	var flag=true;
	 	$.each(inps,function(i, n){
	 		if($(n).val()=="" || $(n).val()=="NaN"){
				$(this).css('border', '3px solid #61533f');
				$(this).next().css('visibility','visible');
				flag=false;
				
	 		}
	 	});
	 	if(flag){
			$.post('<?php echo get_template_directory_uri(); ?>/order.php',
	 		{
	 			start:$('#datepicker-from').val(),
	 			end:$('#datepicker-to').val(),
	 			category:$('option[value='+roomID+']', $('#hotel-room')).text()+' ('+roomID+')',
	 			adults:$('#adults').val(),
	 			child_less12:$('#less12').val(),
	 			extra:$('#extra').val(),
	 			name:$('#name').val(),
	 			fname:$('#surname').val(),
	 			country:$('#country').val(),
	 			tname:$('#patronymic').val(),
	 			comment:$('#coments').val(),
	 			email:$('#email').val(),
	 			sum:$('#total').val(),
	 			phone:$('#phone-number').val()
	 		});
	 		$('#order .thank-you').html('Спасибо за заказ. С Вами свяжутся');
            $('.row-close').show();
            $('.close').hide();
	 	}
	 });
  </script>
  <div id="mask"></div>
</div>
<section id="page">
			<header class="header">
				<section class="header-top">
					<div class="header-top-holder">
						<div class="logo">
						  <?php if(is_page('home')): ?>
              <span></span>
						  <?php else: ?>
						  <a href="<?php bloginfo( 'url' ); ?>" title="Гостиничный комплекс ЦВС ЕВРОПА">Гостиничный комплекс ЦВС ЕВРОПА</a>
						  <?php endif; ?>							
						</div><!--logo-->
						<div class="language">
							<ul>
								<li><a href="<?php bloginfo( 'url' ); ?>/en">Eng</a></li>
								<li class="active"><a href="<?php bloginfo( 'url' ); ?>">Рус</a></li>
							</ul>
						</div><!--lenguage-->
						<nav class="sidebar">
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
						</nav><!--sidebar-->	
					</div><!--header holder-->
				</section><!--header top-->	