<?php get_header(); ?>
<section class="header-bottom">
					<div class="slideshow-holder">
						<nav class="slideshow-nav">
							<div id="nav"></div>
						</nav>
						<div class="slideshow">
							<?php
                if ( $images = get_posts(array(
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'orderby'        => 'title',
                    'order'           => 'ASC',
                    'post_mime_type' => 'image',
                    'exclude' => $thumb_ID,
                    )))
                {
                    foreach( $images as $image ) {
                        $attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
                        echo '<img src="'.$attachmentImage[0].'">';
                    }
                }
              ?>
						</div>
					</div><!--slideshow holder-->
				</section><!--header bottom-->
			</header><!--header-->
			<section class="wrapper">
				<article class="content-bars">
          <?php query_posts('post_type=bar&orderby=date&order=ASC'); while(have_posts()): the_post(); ?>
					<article class="content-holder <?php the_field('block_type') ?>">
						<article class="post-box">
                            <div class="post-box-content">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
                                <p>
                                    <?php the_excerpt(); ?>
                                </p>
                            </div>
						</article>
						<figure class="btn-img">
							<a href="<?php the_permalink(); ?>" class="btn-smoll-right">Подробнее</a>
							<a href="<?php the_permalink(); ?>"><img src="<?php the_field('thumb') ?>" alt="<?php the_title() ?>"></a>
						</figure>
					</article>
          <?php endwhile; ?>
				</article><!--content-->
			</section><!--wrapper-->
		</section><!--page-->
<?php get_footer(); ?>