﻿<?php get_header(); ?>
<!-- <section id="page"> -->
	<!-- <header class="header"> -->
		<section class="header-bottom">
			<div class="slideshow-holder-smoll">
				<nav class="slideshow-nav">
					<div id="nav"></div>
				</nav>
				<div class="slideshow">
					<?php
					if ( $images = get_posts( array(
								'post_parent' => $post->ID,
								'post_type' => 'attachment',
								'numberposts' => -1,
								'orderby' => 'title',
								'order' => 'ASC',
								'post_mime_type' => 'image',
								'exclude' => $thumb_ID,
						)))
					{
						foreach( $images as $image ) {
							$attachmentImage = wp_get_attachment_image_src( $image->ID, 'full' );
							echo '<img src="', $attachmentImage[0], '">';
						}
					}
					?>
				</div>

				<div class="calculator inline">
					<div class="choice">
						<?php get_template_part( 'calculator', 'datepicker' ) ?>
						<?php get_template_part( 'calculator', 'order' ) ?>
                        <div class="middle-choice">
                            <?php get_template_part( 'calculator', 'room' ) ?>
                            <?php get_template_part( 'calculator', 'people' ) ?>
                        </div>
					</div>
				</div>

			</div><!--.slideshow-holder-->
		</section><!--.header-bottom-->
	</header><!--.header-->
	<section class="wrapper">
		<section class="description">
			<?php query_posts('pagename=otel'); while(have_posts()): the_post(); ?>
			<article class="description-left">
				<h2><?php the_field('home_header') ?></h2>
				<p><?php the_field('home_text') ?></p>
				<a href="<?php the_field('price_url') ?>" class="btn-price">Скачать прайс</a>
				<a href="<?php the_permalink() ?>" class="btn-more">Подробнее</a>
			</article>
			<?php endwhile; wp_reset_query(); ?>
			<article class="description-right">
				<?php /*Вывод рубрики интересно*/ query_posts("cat=5,6,7&posts_per_page=3"); while (have_posts()) : the_post(); ?> 
					<figure>
                        <div class="description-right-in">
                            <div class="tr_holder"><div class="tr_date"><?php the_time('d.m.Y'); ?></div><div class="tr_category"><?php the_category(); ?></div></div>
                            <div class="tr_title"><a href="<?php the_permalink(); ?>"><?php trim_title_words(8, ' ...'); ?></a></div>
                            <div class="tr_anons"><?php trim_content_words(24, ' ...'); ?></div>
                        </div>
					</figure>
				<?php endwhile; wp_reset_query(); ?>
			</article>
				<?php /*End of Вывод рубрики интересно*/ ?>
		</section>
		<article class="post">
			<figure>
				<figcaption class="post-smal tr_offers">
				<div style="padding:90px 0  0;">
					<a href="<?php site_url(); ?>/spezpredlozheniya/">Все<br />спецпредложения</a>
				</div>
				</figcaption>
                <div class="post-img-m"><?php the_post_thumbnail() ?></div>
				<figcaption class="post-big">
					<?php the_content() ?>
					<span>Часы работы 08:00 — 01:00</span>
				</figcaption>
                <div class="post-img"><?php the_post_thumbnail() ?></div>
			</figure>
		</article>
		<article class="post">
			<?php query_posts('pagename=kids-club'); while(have_posts()): the_post(); ?>
			<article class="kids_club img img_4">
                <figure class="btn-img">
				    <?php echo get_the_post_thumbnail( null, array(190, 253) ); ?>
                    <a href="<?php site_url(); ?>/category/kids-club" class="button" id="img_4"><span>Подробнее</span></a>
                </figure>
				<div class="tr_kids_klub tr_kids_klub-one">
					<h2><?php the_field('title') ?></h2>
					<p><?php the_field('content') ?></p>
				</div>
			</article>
            <div class="tr_kids_klub tr_kids_klub-m">
                <h2><?php the_field('title') ?></h2>
                <p><?php the_field('content') ?></p>
                <a href="<?php site_url(); ?>/category/kids-club" class="button" id="img_4"><span>Подробнее</span></a>
            </div>

			<?php endwhile; wp_reset_query(); ?>
			
			<div class="tr_video">
				<object width="381" height="253">
					<?php the_field('video_field'); ?>
					<script type="text/javascript">
						$(document).ready(function(){
							$('iframe').width('381px');
							$('iframe').height('253px');
						});
					</script>
				</object>
			</div>
			
		</article>
	</section><!--.wrapper-->
</section><!--#page-->
<?php get_footer(); ?>