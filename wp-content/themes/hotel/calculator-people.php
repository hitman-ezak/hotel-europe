<div class="people-holder">
	<div class="input-holder">
		<label for="adults">
			<p>Взрослых:</p>
		</label>
		<input id="adults" name="adults" maxlength="2" class="number" type="text">
	</div>
	<div class="input-holder">
		<label for="less12">
			<p>от 6 до 12:</p>
		</label>
		<input id="less12" name="less12" maxlength="2" class="number" type="text">
	</div>
	<div class="input-holder">
		<label for="extra">
			<p>от 12:</p>
		</label>
		<input id="extra" name="extra" maxlength="2" class="number" type="text">
	</div>
</div><!--.people-holder-->