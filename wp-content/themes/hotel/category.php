﻿<?php get_header(); ?>
<?php $category = get_the_category();
	  $term_id = $category[0]->term_id;
?>
<section class="header-bottom">
					<div class="category-image-holder">
						<?php
						// vars
						$queried_object = get_queried_object(); 
						$taxonomy = $queried_object->taxonomy;
						$term_id = $queried_object->term_id;  
						 
						// load thumbnail for this taxonomy term
						$cat_image = get_field('category_image', $taxonomy . '_' . $term_id);
						echo '<img src="'.$cat_image.'" />';
						?>
					</div><!--category image holder-->
				</section><!--header bottom-->
			</header><!--header-->
			<section class="wrapper">
				<article class="content-offers">
				<?php  while(have_posts()): the_post(); ob_start(); ?>
				<article class="service-post">
          <h2><a href="<?php the_permalink() ?>"><?php trim_title_words(5, ' ...'); ?></a></h2>
          <p>
            <?php
			if (strlen(get_the_excerpt())>200 && get_field('block_type')=='spa'){
				echo mb_substr(get_the_excerpt(), 0, 200) . '...';
			} elseif(strlen(get_the_excerpt())>200 && get_field('block_type')!='spa'){
				echo mb_substr(get_the_excerpt(), 0, 140) . '...';
			} else {
				the_excerpt();
			}
			?>
          </p>
        </article>
        <?php 
        if(get_field('image_position')=='картинка справа' && get_field('block_type')=='spa') $btn_class = 'btn-big-right';
        if(get_field('image_position')=='картинка справа' && get_field('block_type')!='spa') $btn_class = 'btn-smoll-right';
        if(get_field('image_position')=='картинка слева') $btn_class = 'btn-smoll-left-right';
        ?>
        <?php $content_block = ob_get_clean(); ?>
					<article class="service <?php the_field('block_type') ?>">
						<?php if(get_field('image_position')=='картинка справа') echo $content_block; ?>
						<figure class="btn-img">
							<img src="<?php the_field('thumb') ?>" alt="<?php the_title(); ?>">
							<a href="<?php the_permalink() ?>" class="<?php echo $btn_class ?>">Подробнее</a>
						</figure>
						<?php if(get_field('image_position')=='картинка слева') echo $content_block; ?>
					</article>
					<?php endwhile; ?>
				</article><!--content-->
			</section><!--wrapper-->
		</section><!--page-->
		<?php if ($term_id != 8) {?>
			<script>
				$('#menu-item-612').addClass('current-menu-item');
			</script>
		<?php } ?>
<?php get_footer(); ?>