﻿<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>
<section class="header-bottom">
    <div class="baner">
		<?php 
			$category = get_the_category();
			$term_id = $category[0]->term_id;
			$header_cat_name = $category[0]->name;
			$cat_name = '';
			if ($term_id == 5) $cat_name = 'новости';
			if ($term_id == 6) $cat_name = 'события';
			if ($term_id == 7) $cat_name = 'секреты';
			if ($term_id == 8) $cat_name = 'клубы';
			$cat_image = get_field('category_image', 'category_' . $term_id);
			echo '<img src="'.$cat_image.'">';
		?>
    </div>
</section><!--header bottom-->
</header><!--header-->
	<section class="wrapper">
		<aside class="left-box">
			<nav class="menu offer">
				<h2><?php echo $header_cat_name?></h2>
				<ul>
					<?php $barId = $post->ID ?>
					<?php query_posts('cat='.$term_id.'&posts_per_page=10'); while(have_posts()): the_post(); ?>
						<?php if($barId == $post->ID): ?>
							<li class="active"><?php the_title(); ?></li>
						<?php else: ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endif; ?>
					<?php endwhile; wp_reset_query(); ?>
				</ul>

                <a href="<?php echo get_category_link($term_id)?>" class="btn-all">Все <?php if ($term_id == 8):  echo $cat_name = 'новости'; else: echo $cat_name; endif; ?> </a>
			</nav><!--menu-->
		</aside><!--left-box-->
		<article class="content">
			<article class="post-info">
				<h2><?php the_title() ?></h2>
				<?php the_content() ?>
			</article>
			<!--article class="post-info stock">
				<h2><?php the_title() ?> включает</h2>
				<img src="<?php the_field('thumb') ?>" alt="Пакет «<?php the_title() ?>»">
				<?php the_field('include'); ?>
			</article-->
		</article><!--content-->
		</section><!--wrapper-->
		</section><!--page-->
		<?php endwhile; ?>
		<?php if ($term_id != 8) {?>
			<script>
				$('#menu-item-612').addClass('current-menu-item');
			</script>
		<?php } ?>
		<?php get_footer(); ?>