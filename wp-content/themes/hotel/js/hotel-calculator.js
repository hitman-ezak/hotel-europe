jQuery(function($){
	function updatePrice() {
		setTimeout(function(){
			var $room = $('#hotel-room option:selected');
			var roomID = $room.val();
			var roomTitle = $room.text();
			var from = parseDate( $('#datepicker-from').val() );
			var to = parseDate( $('#datepicker-to').val() );
			var adults = parseInt( $('#adults').val() );
			var extra = parseInt( $('#extra').val() );
			var less12 = parseInt( $('#less12').val() );

			if ( isNaN( extra )) {
				extra = 0;
			}
			if ( isNaN( extra )) {
				extra = 0;
			}
			if ( isNaN( less12 )) {
				less12 = 0;
			}

			var result = 0;

			var costStack = findPriceForJoinedSeasons( from, to );
			var cost = 0;
			for ( i in costStack ) {
				result += sumPrice( costStack[i].season, costStack[i].days, roomID, {
						"adults": adults,
						"extra": extra,
						"less": { "12": less12 }
				});
			}

			if ( isNaN( result )) {
				result = 0;
			}

			$('#total').html( result );
		}, 100);
	}

	function sumPrice ( season, days, roomID, people )
	{
		if ( undefined == people.adults ) {
			people.adults = 0;
		}
		if ( undefined == people.extra ) {
			people.extra = 0;
		}
		if ( undefined == people.less ) {
			people.less = {};
		}

		if ( people.adults > 2 ) {
			people.extra += people.adults - 2;
			people.adults = 2;
		}

		var cost = season.rooms[roomID].cost;
		var sum = 0;
		// Стоимость за основные места
		sum += parseInt( cost.forPlace[people.adults] );
		// Стоимость за дополнительные места
		sum += parseInt( cost.extra * people.extra );
		// Стоимость для особого возраста
		sum += parseInt( cost.lessAge['12'] * people.less['12'] );

		// Умножаем сумму на количество дней
		sum *= days;

		return sum;
	}

	function findPriceForJoinedSeasons ( from, to )
	{
		// Найдем первый сезон который подходит для заданого периода
		var milisecondInTheDay = 86400;
		var costStack = [];
		var diffType = 'from';

		var dayCount = 0;
		var firstTrip = true;
		for ( id in price ) {
			if ( firstTrip ) {
				firstTrip = false;
				if ( from < price[id]['time']['from'] ) {
					var date = convertTimeToDate( price[id]['time']['from'] );
					// TODO:
					// reportWarning( 'Заказы будут только лишь с ' + date );
					return false;
				}
			}

			if ( diffType == 'from' ) {
				if ( price[id]['time']['from'] <= from && from <= price[id]['time']['to'] ) {

					if ( price[id]['time']['to'] < to ) {
						dayCount = price[id]['time']['to'] - from;
					} else {
						 dayCount = to - from;
					}
					dayCount /= milisecondInTheDay;

					if ( price[id]['time']['to'] < to ) {
						dayCount++;
					}

					costStack.push({
						"days": dayCount,
						"season": price[id]
					});
					diffType = 'to';
				}
			} else {
				if ( price[id]['time']['from'] <= to ) {
					if ( price[id]['time']['to'] < to ) {
						dayCount = price[id]['time']['to'] - price[id]['time']['from'];
					} else {
						if ( price[id]['time']['to'] >= to ) {
							dayCount = to - price[id]['time']['from'];
						}
					}
					dayCount /= milisecondInTheDay;

					if ( price[id]['time']['to'] < to ) {
						dayCount++;
					}

					costStack.push({
						"days": dayCount,
						"season": price[id]
					});
				} else {
					break;
				}
			}
		}

		if ( price[id]['time']['to'] < to ) {
			var date = convertTimeToDate( price[id]['time']['to'] );
			// TODO:
			// reportWarning( 'Вы не можете забронировать номер на дату после ' + date );
			return false;
		}

		return costStack;
	}

	/* =====================================
	 * Binds
	 * ===================================== */

	/* Update price
	---------------------------------------- */
	$('body').on( 'mouseup','.dropdown li, .ui-datepicker-calendar a, #hotel-room, .hasDatepicker', updatePrice );
	$('.calculator input').keyup( updatePrice );

	/* Setup datepickers
	---------------------------------------- */
	var dpFrom = $('#datepicker-from');
	var dpTo = $('#datepicker-to');

	var today = new Date();
	today = today.getTime();
	today = ( today - ( today % (86400 * 1000 ))) / 1000;

	firstPriceID = null;
	for ( priceID in price ) {
		if ( firstPriceID == null ) {
			firstPriceID = priceID;
		}
	}

    if(price != 0){
        var maxDpDate = dayDiff( today, price[priceID].time.to );
        var minDpDate = 0;
        if ( today < price[firstPriceID].time.from) {
            minDpDate = dayDiff( today, price[firstPriceID].time.from );
        }
        var dpOptions = { showOn: "both", minDate: '+' + minDpDate + 'd', maxDate: '+' + maxDpDate + 'd' };

        dpFrom.datepicker( dpOptions );
        dpTo.datepicker( dpOptions );
        $('body').on( 'mouseup','.ui-datepicker-calendar', updateDatepickers );
        // $('body').on( 'mouseup','#datepicker-from, #datepicker-to', updateDatepickers );
        $('body').on( 'keyup','#datepicker-from, #datepicker-to', updateDatepickers );
    }
	function updateDatepickers(e)
	{
		setTimeout(function(){
			var from = parseDate( dpFrom.val() );
			from = dayDiff( today, from ) + 1;
			var to = parseDate( dpTo.val() );
			to = dayDiff( today, to ) - 1;

			var optionsForFrom = dpOptions;
			if ( isNaN( to )) {
				to = maxDpDate;
			}
			dpFrom.datepicker( 'option', 'maxDate', '+' + to + 'd' );

			var optionsForTo = dpOptions;
			if ( isNaN( from )) {
				from = minDpDate;
			}
			dpTo.datepicker( 'option', 'minDate', '+' + from + 'd' );
		}, 100);
	}

	/* Order permit
	---------------------------------------- */
 /* $('#order input').blur(function() {
   if($(this).val().length == 0) {
 	$(this)
 	  .css('border','2px solid black')
 	  .after('<div style="color:red;">* - обязательное поле!</div>');
	  }
	 });
	 $('#order input').focus(function() {
	   $(this).next('div').remove();
	   $(this).css('border', '');
	   });
	 $('.btn-search').click(function(){
	 	var roomID = $('#hotel-room').val();
	 	var inps=$("#order input");
		
	 	var flag=true;
	 	$.each(inps,function(i, n){
	 		if($(n).val()=="" || $(n).val()=="NaN"){
				flag=false;
				
	 		}
	 	});
	 	if(flag){
			$.post(theme_dir + '/order.php',
	 		{
	 			start:$('#datepicker-from').val(),
	 			end:$('#datepicker-to').val(),
	 			category:$('option[value='+roomID+']', $('#hotel-room')).text()+' ('+roomID+')',
	 			adults:$('#adults').val(),
	 			child_less12:$('#less12').val(),
	 			extra:$('#extra').val(),
	 			name:$('#name').val(),
	 			fname:$('#fname').val(),
	 			country:$('#country').val(),
	 			tname:$('#tname').val(),
	 			comment:$('#comment').val(),
	 			email:$('#email').val(),
	 			sum:$('#total').val(),
	 			phone:$('#phone').val()
	 		});
	 		$('#order .inner').html('Спасибо за заказ. С Вами свяжутся');
	 		$('#order .inner').html($("#dialog").html());
	 	}
	 }); */

	/* Сообщает о том, что номер недоступен
	 ---------------------------------------- */
	reportAboutUnavailableRoom = function( roomCategoryTitle, date )
	{
		var modalId = '#warningModal';
		var modal = $(modalId);

		if ( undefined == date.from ) {
			date.from = '';
		} else {
			date.from = ' c ' + date.from;
		}
		if ( undefined == date.to ) {
			date.to = '';
		} else {
			date.to = ' до ' + date.to;
		}

		var message = '<p>В период'
			+ date.from + date.to
			+ ' номера категории '
			+ roomCategoryTitle
			+ ' - недоступны.</p>';

		reportWarning( message );
	}
});