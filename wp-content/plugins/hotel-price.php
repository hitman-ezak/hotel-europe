<?php
/*
Plugin Name: Hotel Price
Plugin URI: http://www.pink.in.ua/
Description: Управление прайсом
Author URI: http://www.pink.in.ua/
Author: P.I.N.K.
Version: 1.0.0
*/
$hotel_price_name = "Прайс сезонов";

function hotel_price_code_add_admin()
{
	global $hotel_price_name;
	add_options_page( $hotel_price_name, $hotel_price_name, 'edit_themes', basename(__FILE__), 'hotel_price_code_to_admin' );
}
// Вид административной страницы и обработка-запоминание получаемых опций
function hotel_price_code_to_admin()
{
	global $hotel_price_name;

	$rooms = $rooms = get_posts( array(
			'post_type' => 'room',
			'numberposts' => -1,
			'orderby' => 'date',
			'order' => 'ASC',
	));
	?>
		<div class="wrap">
			<?php
			screen_icon(); // Значок сгенерируется автоматически
			echo '<h2>', $hotel_price_name, '</h2>'; // Заголовок
			$price = objectToArray( json_decode( get_option( 'hp' )));
			// Пошла обработка запроса
			if (isset($_POST['save'])) {
				$price = $_POST['hp'];
				$price = hotel_price_set_time_formats( hotel_price_sort( $price ));
				// Конвертация прайса и сохранение
				update_option('hp', json_encode( $price ));

				echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>'.__('Settings saved.').'</b></p></div>';
			}

			$priceNum = count( $price );
			// Внешний вид формы
			?>
			<form method="post" id="price-list">
				<ol>
					<?php foreach ($price as $priceID => $season) : $namePrefix="hp[".$priceID."]"; ?>
						<li>
							<h3>
								<input type="text" class="title" placeholder="Название сезона" name="<?php echo $namePrefix ?>[title]" value="<?php echo $season['title'] ?>"> 
								(<input type="text" class="datepicker" placeholder="Начало" name="<?php echo $namePrefix ?>[time][from]" value="<?php echo date('d-m-Y', $season['time']['from']) ?>">
								 - <input type="text" class="datepicker" placeholder="Конец" name="<?php echo $namePrefix ?>[time][to]" value="<?php echo date('d-m-Y', $season['time']['to']) ?>">) 
								<span data-toggle="#price-<?php echo $priceID ?>">изменить</span> 
								<a href="javascript:removePrice(<?php echo $priceID ?>)" class="remove">удалить</a>
							</h3>

							<div id="price-<?php echo $priceID ?>" class="price-table">
								<table>
									<thead>
										<tr>
											<th>Категория номера</th>
											<th>1 чел</th>
											<th>2 чел</th>
											<th>Доп. места</th>
											<th>Младше 12-ти</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ( $rooms as $room ): ?>
											<tr>
												<th><?php echo $room->post_title ?></th>
												<td><input type="text" name="<?php echo $namePrefix ?>[rooms][<?php echo $room->ID ?>][cost][forPlace][1]" value="<?php echo $season['rooms'][$room->ID]['cost']['forPlace'][1] ?>"></td>
												<td><input type="text" name="<?php echo $namePrefix ?>[rooms][<?php echo $room->ID ?>][cost][forPlace][2]" value="<?php echo $season['rooms'][$room->ID]['cost']['forPlace'][2] ?>"></td>
												<td><input type="text" name="<?php echo $namePrefix ?>[rooms][<?php echo $room->ID ?>][cost][extra]" value="<?php echo $season['rooms'][$room->ID]['cost']['extra'] ?>"></td>
												<td><input type="text" name="<?php echo $namePrefix ?>[rooms][<?php echo $room->ID ?>][cost][lessAge][12]" value="<?php echo $season['rooms'][$room->ID]['cost']['lessAge'][12] ?>"></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</li>
					<?php endforeach ?>
				</ol>
				<div class="submit">
					<input name="add" type="button" class="button" value="Добавить сезон" />
					<input name="save" type="submit" class="button-primary" value="Сохранить изменения" />
				</div>
			</form>
		</div>
	<?php

	hotel_price_print_style();
	hotel_price_print_script( $rooms, $priceID );
}

// Итоговые действия
add_action('admin_menu', 'hotel_price_code_add_admin');
// Никаких следов после деинсталляции
// О работе хука я уже писал в одной из своих предыдущих записей
if (function_exists('register_uninstall_hook'))
	register_uninstall_hook(__FILE__, 'hotel_price_deinstall');

function hotel_price_deinstall()
{
	delete_option('hotel_price');
}

function hotel_price_print_style()
{
	?>
		<style type="text/css">
			#price-list .price-table {
				display: none;
			}
			#price-list th {
				background: #eee;
				padding: 4px 10px;
			}
			#price-list h3 input {
				width: 85px;
				text-align: center;
				border-width: 0;
				border-bottom-width: 1px;
			}
			#price-list h3 input.title {
				width: 170px;
			}
			#price-list h3 a,
			#price-list h3 span {
				color: #21759b;
				cursor: pointer;
				font-weight: normal;
				text-decoration: none;
				border-bottom: 1px dotted;
				font-size: 13px;
			}
			#price-list h3 a.remove {
				color: rgb(219, 0, 0);
			}
		</style>
	<?php
}

function hotel_price_print_script( $rooms, $priceID )
{
	?>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script>
			var lastPriceID = <?php echo ++$priceID ?>;
			jQuery('.datepicker').datepicker({
				inline: true,
				dateFormat: "dd-mm-yy"
			});
			jQuery(function($){
				$('#price-list').on('click', 'h3 span', function(e){
					$($(this).attr('data-toggle')).toggle();
				});
				$('#price-list').on('click', 'input[name=add]', function(e){
					var namePrefix="hp["+lastPriceID+"]";
					var title = '<h3><input class="title" type="text" name="'+namePrefix+'[title]" value="" placeholder="Название сезона"> (<input type="text" name="'+namePrefix+'[time][from]" class="datepicker" value="" placeholder="Начало"> - <input type="text" name="'+namePrefix+'[time][to]" class="datepicker" value="" placeholder="Конец">) <span data-toggle="#price-' + lastPriceID + '">изменить</span>   <a href="javascript:removePrice(' + lastPriceID + ')" class="remove">удалить</a></h3>';
					var table = '<div id="price-' + lastPriceID + '" class="price-table" style="display:block;">'
								+ '<table><thead><tr>'
											+ '<th>Категория номера</th>'
											+ '<th>1 чел</th>'
											+ '<th>2 чел</th>'
											+ '<th>Доп. места</th>'
											+ '<th>Младше 12-ти</th>'
								+ '</tr></thead><tbody>'
										<?php foreach ( $rooms as $room ): ?>
											+ '<tr>'
												+ '<th><?php echo $room->post_title ?></th>'
												+ '<td><input name="'+namePrefix+'[rooms][<?php echo $room->ID ?>][cost][forPlace][1]" type="text"></td>'
												+ '<td><input name="'+namePrefix+'[rooms][<?php echo $room->ID ?>][cost][forPlace][2]" type="text"></td>'
												+ '<td><input name="'+namePrefix+'[rooms][<?php echo $room->ID ?>][cost][extra]" type="text"></td>'
												+ '<td><input name="'+namePrefix+'[rooms][<?php echo $room->ID ?>][cost][lessAge][12]" type="text"></td>'
											+ '</tr>'
										<?php endforeach ?>
									+ '</tbody>'
								+ '</table>'
							+ '</div>';
					$('<li></li>')
						.append(title)
						.append(table)
						.appendTo('#price-list ol');
					lastPriceID++;
				});
			});

			function removePrice ( priceID )
			{
				var confirmed = confirm( 'Удалить сезон?' );
				if ( confirmed ) {
					jQuery('#price-'+priceID).parent().remove();
				}
			}
		</script>
	<?php
}

function hotel_price_sort( $price )
{
	ksort( $price );
	foreach ( $price as &$season ) {
		ksort( $season['rooms'] );
		foreach ( $season['rooms'] as &$room ) {
			ksort( $room['cost']['forPlace'] );
			ksort( $room['cost']['lessAge'] );
		}
	}

	return $price;
}

function hotel_price_set_time_formats( $price )
{
	$temp_price = array();

	foreach ( $price as $id => $season ) {
		$price[ $id ]['time']['from'] = strtotime( $season['time']['from'] );
		$price[ $id ]['time']['to'] = strtotime( $season['time']['to'] );

		if ( $id != $price[ $id ]['time']['from'] ) {
			$temp_price[ $price[ $id ]['time']['from'] ] = $price[ $id ];
		} else {
			$temp_price[ $id ] = $price[ $id ];
		}
	}

	return $temp_price;
}